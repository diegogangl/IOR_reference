# IOR Reference Addon

![](screen.png)

IOR Reference is an addon for Blender that adds a list of common IOR values for
materials in the nodes editor. It also includes a button to insert a value node
with the IOR.

And as with all UILists in Blender, you can also search and sort the list by clicking the plus icon at the bottom.


## Instaling

Download the Python file. Open Blender and go to Edit > Preferences > Add-ons. Click the "Install button" and point to the file you've downloaded. Enable the add-on by clicking the checkbox next to its name.

# Using

Whenever you are editing materials in the shader editor, press N to open the side panel. Look for the IOR Reference panel.


